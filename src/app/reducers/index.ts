import {
  Action,
  ActionReducer,
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { merge } from 'lodash';

export interface AppState {
}

export function sessionStorageMergeMetaReducer<S, A extends Action = Action>(reducer: ActionReducer<S, A>) {
  let onInit = true;
  return function(state: S, action: A): S {
    const nextState = reducer(state, action);
    // init the application state.
    const stateString = localStorage.getItem('registrar.state')
    const savedState = stateString ? JSON.parse(stateString) : {};
    if (onInit) {
      onInit  = false;
      return merge(nextState, savedState);
    }
    const setState = merge(savedState, nextState);
    sessionStorage.setItem('registrar.state', JSON.stringify(setState));
    return nextState;
  };
}

export function localStorageMergeMetaReducer<S, A extends Action = Action>(reducer: ActionReducer<S, A>) {
  let onInit = true;
  return function(state: S, action: A): S {
    const nextState = reducer(state, action);
    // init the application state.
    const stateString = localStorage.getItem('registrar.state')
    const savedState = stateString ? JSON.parse(stateString) : {};
    if (onInit) {
      onInit  = false;
      return merge(nextState, savedState);
    }
    const setState = merge(savedState, nextState);
    localStorage.setItem('registrar.state', JSON.stringify(setState));
    return nextState;
  };
}

export const reducers: ActionReducerMap<AppState> = {
};

export const metaReducers: MetaReducer<AppState>[] = [
  sessionStorageMergeMetaReducer
];
